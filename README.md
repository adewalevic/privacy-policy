

* Valencia, Spain. 18/10/2021
* caminandum is a community of persons who like to do outdoor sport whith nearby persons and operates :
* caminandum app for Android and iOS.
* caminandum.com website
* caminandum sport radio,  (the «Service»).
* This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.
* We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy. Unless otherwise defined in this privacy policy, terms used in this privacy policy have the same meanings as in our terms and conditions, accessible from our website caminandum.com

* Information collection and use
* We collect several different types of information for various purposes to provide and improve our Service to you.
* Personal data
* While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you («Personal Data»). Personally identifiable information may include, but is not limited to:
* – First name
* – User picture
* – Gender
* – Age
* – Adress
* – Outdoor Sports you like
* – Payment information for subcription
* Transfer of data
* Your information, including personal data, may be maintained on computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.
* If you are located outside of France and choose to provide information to us, please note that we transfer the data, including personal data, to ovh.com in France and process it there.
* Your consent to this privacy policy followed by your submission of such information represents your agreement to that transfer.
* caminandum will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy and no transfer of your personal data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.

* Disclosure of data. Legal requirements
* caminandum may disclose your personal data in the good faith belief that such action is necessary to:
* – To comply with a legal obligation.
* – To protect and defend the rights or property of caminandum.
* – To prevent or investigate possible wrongdoing in connection with the service.
* – To protect the personal safety of users of the service or the public.
* – To protect against legal liability.
* Security of data
* The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your personal data, we cannot guarantee its absolute security.

* Service providers
* We employ third party companies ovh.com to facilitate our Service («service providers»), to provide the service on our behalf, to perform service-related services or to assist us in analyzing how our service is used.
* We use an Open Source tracking service to better know you named Matomo.
* We don´t use GPS for user geolocation but IP geolocation which is less precise.
* ovh.com has access to your personal data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.
* Links to other sites
* Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party’s site. We strongly advise you to review the privacy policy of every site you visit.
* We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.

* Children’s privacy
* Our Service does address anyone older than 16. We may collect name, lastname, picture information from anyone older than 16. If you are a parent or guardian, you are aware that your children has provided us with personal data.
* Changes to this privacy policy
* We may update our privacy policy from time to time. We will notify you of any changes by posting the new privacy policy on this page.
* You are advised to review this privacy policy periodically for any changes. Changes to this privacy policy are effective when they are posted on this page.
* Contact Us
* If you have any questions about this privacy policy, please contact us:
* – By email : privacy@caminandum.com
* – Call us: (+34) 962 071 229
* – Write us: caminandum. Carrer de la Reina, 6. 46011 Valencia. Spain